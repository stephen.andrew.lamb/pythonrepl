﻿using System.Collections.Generic;
using System.Linq;
using IronPython.Compiler.Ast;

namespace PythonRepl.Python.SyntaxHighlight
{
    public static partial class AstNodeSyntaxHighlighters
    {
        private static IEnumerable<TextChunk> AndExpression(AndExpression andExpression, int indent = 0)
            => LeftRightExpression(andExpression.Left, andExpression.Right, Chunk(" and ", TokenType.Operator));

        private static IEnumerable<TextChunk> OrExpression(OrExpression orExpression, int indent = 0)
            => LeftRightExpression(orExpression.Left, orExpression.Right, Chunk(" or ", TokenType.Operator));

        private static IEnumerable<TextChunk> BinaryExpression(BinaryExpression binaryExpression, int indent = 0)
            => LeftRightExpression(binaryExpression.Left, binaryExpression.Right, Chunk($" {Operator(binaryExpression.Operator)} ", TokenType.Operator));

        private static IEnumerable<TextChunk> TupleExpression(TupleExpression tupleExpression, int indent = 0)
            => AddList(_leftBrace, _rightBrace, _comma, tupleExpression.Items.Select(i => (Node)i));

        private static IEnumerable<TextChunk> ParenthesisExpression(ParenthesisExpression parenthesisExpression, int indent = 0)
        {
            List<TextChunk> result = new List<TextChunk>();

            result.Add(_leftBrace);
            result.AddRange(parenthesisExpression.Expression.HighlightCode());
            result.Add(_rightBrace);

            return result;
        }

        private static IEnumerable<TextChunk> NameExpression(NameExpression expression, int indent = 0)
            => new List<TextChunk>() { Chunk($"{Indent(indent)}{expression.Name}", TokenType.Variable) };

        private static IEnumerable<TextChunk> ListExpression(ListExpression listExpression, int indent = 0)
            => AddList(_leftSquare, _rightSquare, _comma, listExpression.Items.Select(p => (Node)p));

        private static IEnumerable<TextChunk> SetExpression(SetExpression setExpression, int indent = 0)
            => AddList(_leftSquare, _rightSquare, _comma, setExpression.Items.Select(p => (Node)p));

        private static IEnumerable<TextChunk> DictionaryExpression(DictionaryExpression dictionaryExpression, int indent = 0)
            => AddList(_leftMoustache, _rightMoustache, _comma, dictionaryExpression.Items.Select(p => (Node)p));

        private static IEnumerable<TextChunk> SliceExpression(SliceExpression sliceExpression, int indent = 0)
            => LeftRightExpression(sliceExpression.SliceStart, sliceExpression.SliceStop, Chunk(" : ", TokenType.Operator));

        private static IEnumerable<TextChunk> ConstantExpression(ConstantExpression expression, int indent = 0)
        {
            TokenType tokenType = TokenType.Number;

            string value = null != expression.Value ? expression.Value.ToString()
                                                    : "None"; 

            if (expression.Type == typeof(string))
            {
                tokenType = TokenType.String;
                value = $"\"{expression.Value}\"";
            }

            List<TextChunk> result = new List<TextChunk>() { Chunk($"{value}", tokenType) };

            return result;
        }

        private static IEnumerable<TextChunk> CallExpression(CallExpression expression, int indent = 0)
        {
            List<TextChunk> result = new List<TextChunk>();

            result.AddRange(expression.Target.HighlightCode(indent));
            result.AddRange(AddList(_leftBrace, _rightBrace, _comma, expression.Args.Select(p => (Node)p)));

            return result;
        }

        private static IEnumerable<TextChunk> IndexExpression(IndexExpression expression, int indent = 0)
        {
            List<TextChunk> result = new List<TextChunk>();

            result.AddRange(expression.Target.HighlightCode(indent));
            result.Add(_leftSquare);
            result.AddRange(expression.Index.HighlightCode(indent));
            result.Add(_rightSquare);

            return result;
        }

        private static TextChunk _leftBrace         = Chunk("(", TokenType.Brace);
        private static TextChunk _rightBrace        = Chunk(")", TokenType.Brace);
        private static TextChunk _leftSquare        = Chunk("[", TokenType.Brace);
        private static TextChunk _rightSquare       = Chunk("]", TokenType.Brace);
        private static TextChunk _leftMoustache     = Chunk("{", TokenType.Brace);
        private static TextChunk _rightMoustache    = Chunk("}", TokenType.Brace);
        private static TextChunk _comma             = Chunk(", ", TokenType.Brace);
        private static TextChunk _dot               = Chunk(".", TokenType.Brace);

        private static TextChunk _blank             = Chunk("", TokenType.Brace);
        private static TextChunk _colon             = Chunk(":", TokenType.Operator);
        private static TextChunk _space             = Chunk(" ", TokenType.Brace);
        private static TextChunk _colonSpace        = Chunk(": ", TokenType.Operator);
        private static TextChunk _newLine           = Chunk("\n", TokenType.Operator);

        private static IEnumerable<TextChunk> MemberExpression(MemberExpression expression, int indent = 0)
        {
            List<TextChunk> result = new List<TextChunk>();

            result.AddRange(expression.Target.HighlightCode(indent));
            result.Add(_dot);
            result.Add(Chunk(expression.Name));

            return result;
        }
    }
}
