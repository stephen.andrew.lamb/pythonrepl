﻿using System;
using System.Collections.Generic;
using System.Linq;
using IronPython.Compiler.Ast;

namespace PythonRepl.Python.SyntaxHighlight
{
    public static partial class AstNodeSyntaxHighlighters
    {
        private static IEnumerable<TextChunk> SuiteStatement(SuiteStatement suiteStatement, int indent = 0)
        {
            List<TextChunk> result = new List<TextChunk>();

            foreach (Statement item in suiteStatement.Statements)
            {
                result.AddRange(item.HighlightCode(indent));
                result.Add(_newLine);
            }
            return result;
        }

        public static IEnumerable<TextChunk> AssignmentStatement(AssignmentStatement statement, int indent = 0)
        {
            List<TextChunk> result = new List<TextChunk>();

            foreach (Expression expression in statement.Left)
            {
                result.AddRange(expression.HighlightCode(indent));
            }
            result.Add(Chunk(" = ", TokenType.Operator));
            result.AddRange(statement.Right.HighlightCode(indent));

            return result;
        }

        private static IEnumerable<TextChunk> ExpressionStatement(ExpressionStatement expressionStatement, int indent = 0)
            => expressionStatement.Expression.HighlightCode(indent);

        private static IEnumerable<TextChunk> ReturnStatement(ReturnStatement returnStatement, int indent = 0)
        {
            List<TextChunk> result = new List<TextChunk>();

            result.Add(Chunk($"{Indent(indent)}return "));
            result.AddRange(returnStatement.Expression.HighlightCode());

            return result;
        }

        private static IEnumerable<TextChunk> FromImportStatement(FromImportStatement fromImportStatement, int indent = 0)
        {
            List<TextChunk> result = new List<TextChunk>();
            result.Add(Chunk($"from "));
            result.Add(Chunk($"{string.Join(".", fromImportStatement.Root.Names)} ", TokenType.Operator));
            result.Add(Chunk($"import "));

            result.Add(Chunk($"{string.Join(".", fromImportStatement.Names)} ", TokenType.Operator));
            return result;
        }

        private static IEnumerable<TextChunk> WhileStatement(WhileStatement whileStatement, int indent = 0)
        {
            List<TextChunk> result = new List<TextChunk>();

            result.Add(Chunk($"{Indent(indent)}while ", TokenType.Method));
            result.AddRange(whileStatement.Test.HighlightCode());
            result.Add(_colon);
            result.Add(_newLine);
            result.AddRange(whileStatement.Body.HighlightCode(indent + 1));

            return result;
        }

        private static IEnumerable<TextChunk> IfStatement(IfStatement ifStatement, int indent = 0)
        {
            List<TextChunk> result = new List<TextChunk>();

            result.Add(Chunk($"{Indent(indent)}if ", TokenType.Method));
            result.AddRange(ifStatement.Tests.FirstOrDefault()?.Test.HighlightCode());
            result.Add(_colon);
            result.Add(_newLine);
            result.AddRange(ifStatement.Tests.FirstOrDefault()?.Body.HighlightCode(indent + 1));
            return result;
        }

        private static IEnumerable<TextChunk> ForStatement(ForStatement forStatement, int indent = 0)
        {
            List<TextChunk> result = new List<TextChunk>();

            result.Add(Chunk($"{Indent(indent)}for ", TokenType.Method));
            result.AddRange(forStatement.Left.HighlightCode());
            result.Add(Chunk(" in ", TokenType.DefClass));
            result.AddRange(forStatement.List.HighlightCode());
            result.Add(_colon);
            result.Add(_newLine);
            result.AddRange(forStatement.Body.HighlightCode(indent + 1));
            return result;
        }

        private static IEnumerable<TextChunk> PrintStatement(PrintStatement printStatement, int indent = 0)
        {
            List<TextChunk> result = new List<TextChunk>();

            result.Add(Chunk($"{Indent(indent)}print ", TokenType.Method));
            result.AddRange(AddList(_blank, _blank, _comma, printStatement.Expressions.Select(i => (Node)i)));

            return result;
        }

        private static IEnumerable<TextChunk> AugmentedAssignStatement(AugmentedAssignStatement statement, int indent = 0)
        {
            List<TextChunk> result = new List<TextChunk>();

            result.AddRange(statement.Left.HighlightCode(indent));
            result.Add(Chunk($" {Operator(statement.Operator)}= ", TokenType.Operator));
            result.AddRange(statement.Right.HighlightCode(indent));

            return result;
        }
    }
}
