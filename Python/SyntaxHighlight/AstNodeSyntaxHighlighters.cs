﻿using System;
using System.Collections.Generic;
using System.Linq;
using IronPython.Compiler;
using IronPython.Compiler.Ast;

namespace PythonRepl.Python.SyntaxHighlight
{
    public class TextChunk
    {
        public TextChunk(string text, TokenType type = TokenType.Variable)
        {
            Text = text;
            Color = TokenColors.Colors[type];
        }

        public string Text { get; }
        public ConsoleColor Color { get; }
    }

    public static partial class AstNodeSyntaxHighlighters
    {
        private static Dictionary<PythonOperator, string> _operators = new Dictionary<PythonOperator, string>()
        {
            { PythonOperator.Add,                   "+" },
            { PythonOperator.Divide,                "/" },
            { PythonOperator.Equals,                "==" },
            { PythonOperator.NotEqual,              "!=" },
            { PythonOperator.GreaterThan,           ">" },
            { PythonOperator.GreaterThanOrEqual,    ">=" },
            { PythonOperator.LessThan,              "<" },
            { PythonOperator.LessThanOrEqual,       "<=" },
            { PythonOperator.Multiply,              "*" },
            { PythonOperator.Subtract,              "-" }
        };

        private static Dictionary<string, Func<object, int, IEnumerable<TextChunk>>> _nodeTypes = new Dictionary<string, Func<object, int, IEnumerable<TextChunk>>>()
        {
            { "ExpressionStatement",    (e, i) => ExpressionStatement   (e as ExpressionStatement, i) },
            { "AssignmentStatement",    (e, i) => AssignmentStatement   (e as AssignmentStatement, i) },
            { "SuiteStatement",         (e, i) => SuiteStatement        (e as SuiteStatement, i) },
            { "PrintStatement",         (e, i) => PrintStatement        (e as PrintStatement, i) },
            { "ForStatement",           (e, i) => ForStatement          (e as ForStatement, i) },
            { "IfStatement",            (e, i) => IfStatement           (e as IfStatement, i) },
            { "WhileStatement",         (e, i) => WhileStatement        (e as WhileStatement, i) },
            { "FromImportStatement",    (e, i) => FromImportStatement   (e as FromImportStatement, i) },
            { "ReturnStatement",        (e, i) => ReturnStatement       (e as ReturnStatement, i) },
            { "AugmentedAssignStatement", (e, i) => AugmentedAssignStatement(e as AugmentedAssignStatement, i) },

            { "FunctionDefinition",     (e, i) => FunctionDefinition    (e as FunctionDefinition, i) },

            { "CallExpression",         (e, i) => CallExpression        (e as CallExpression, i) },
            { "NameExpression",         (e, i) => NameExpression        (e as NameExpression, i) },
            { "ListExpression",         (e, i) => ListExpression        (e as ListExpression, i) },
            { "SetExpression",          (e, i) => SetExpression         (e as SetExpression, i) },
            { "DictionaryExpression",   (e, i) => DictionaryExpression  (e as DictionaryExpression, i) },
            { "ConstantExpression",     (e, i) => ConstantExpression    (e as ConstantExpression, i) },
            { "IndexExpression",        (e, i) => IndexExpression       (e as IndexExpression, i) },
            { "SliceExpression",        (e, i) => SliceExpression       (e as SliceExpression, i) },
            { "BinaryExpression",       (e, i) => BinaryExpression      (e as BinaryExpression, i) },
            { "ParenthesisExpression",  (e, i) => ParenthesisExpression (e as ParenthesisExpression, i) },
            { "LambdaExpression",       (e, i) => LambdaExpression      (e as LambdaExpression, i) },
            { "TupleExpression",        (e, i) => TupleExpression       (e as TupleExpression, i) },

            { "MemberExpression",       (e, i) => MemberExpression      (e as MemberExpression, i) },

            { "Arg",                    (e, i) => Arg                   (e as Arg, i) },
            { "Parameter",              (e, i) => Parameter             (e as Parameter, i) },
            { "AndExpression",          (e, i) => AndExpression         (e as AndExpression, i) },
            { "OrExpression",           (e, i) => OrExpression          (e as OrExpression, i) },
            { "UnaryExpression",        (e, i) => UnaryExpression       (e as UnaryExpression, i) },
            
        };

        private static IEnumerable<TextChunk> UnaryExpression(UnaryExpression unaryExpression, int indent = 0)
            => unaryExpression.Expression.HighlightCode();

        private static IEnumerable<TextChunk> LeftRightExpression(Expression left, Expression right, TextChunk middle)
        {
            List<TextChunk> result = new List<TextChunk>();

            result.AddRange(left.HighlightCode());
            result.Add(middle);
            result.AddRange(right.HighlightCode());

            return result;
        }


        private static string Operator(PythonOperator @operator)
        {
            string result = @operator.ToString();
            if(true == _operators.ContainsKey(@operator)) {
                result = _operators[@operator];
            }
            return result;
        }
      
        private static IEnumerable<TextChunk> LambdaExpression(LambdaExpression lambdaExpression, int indent = 0)
        {
            List<TextChunk> result = new List<TextChunk>();

            if(lambdaExpression.Parent is PythonAst)
            {
                PythonAst ast = lambdaExpression.Parent as PythonAst;
                if(ast.Body is AssignmentStatement)
                {
                    AssignmentStatement body = (AssignmentStatement)ast.Body;
                    var left = body.Left[0] as NameExpression;
                    var right = body.Right as LambdaExpression;
                    if(default(FunctionDefinition) != right?.Function)
                    {
                        AstNodeExpressionGenerators.Functions[left.Name] = right.Function;
                    }
                }
            }

            result.Add(Chunk($"lambda ", TokenType.Method));

            result.AddRange(AddList(_blank, _space, _comma, lambdaExpression.Function.Parameters.Select(p => (Node)p)));
            result.Add(Chunk(": ", TokenType.Operator));
            result.AddRange(lambdaExpression.Function.Body.HighlightCode());

            return result;
        }

        #region Generic Run Code Methods
        public static List<TextChunk> HighlightCode(this Expression expression, int indent = 0) => RunCode(expression, indent);
        public static List<TextChunk> HighlightCode(this System.Linq.Expressions.Expression expression, int indent = 0) => RunCode(expression, indent);
        private static List<TextChunk> RunCode(object expression, int indent = 0)
        {
            List<TextChunk> result = new List<TextChunk>();
            string nodeType = expression.GetType().ToString().Substring(24);
            if (_nodeTypes.ContainsKey(nodeType))
            {
                result.AddRange(_nodeTypes[nodeType](expression, indent));
            }
            else
            {
                ConsoleUtils.WriteLine($"{nodeType} not found", ConsoleColor.Red);
            }

            return result;
        }
        #endregion

        private static IEnumerable<TextChunk> Parameter(Parameter parameter, int indent = 0)
        {
            List<TextChunk> result = new List<TextChunk>() { new TextChunk(parameter.Name, TokenType.Variable) };

            return result;
        }

        private static string Indent(int indent)
        {
            return "".PadLeft(indent * 2);
        }



        private static IEnumerable<TextChunk> FunctionDefinition(FunctionDefinition functionDefinition, int indent = 0)
        {
            List<TextChunk> result = new List<TextChunk>();

            result.Add(Chunk("def ", TokenType.DefClass));
            result.Add(Chunk(functionDefinition.Name, TokenType.Method));

            AstNodeExpressionGenerators.Functions[functionDefinition.Name] = functionDefinition;

            result.AddRange(AddList(_leftBrace, _rightBrace, _comma, functionDefinition.Parameters.Select(p => (Node)p)));

            result.Add(_colon);
            result.Add(Chunk("\n"));
            result.AddRange(functionDefinition.Body.HighlightCode(indent + 1));
            return result;
        }

        //private static IEnumerable<TextChunk> AddList(string leftBrace, string rightBrace, string seperator, IEnumerable<Node> items)
        //    => AddList(Chunk(leftBrace, TokenType.Brace), Chunk(rightBrace, TokenType.Brace), Chunk(seperator, TokenType.Brace), items);

        private static IEnumerable<TextChunk> AddList(TextChunk leftBrace, TextChunk rightBrace, TextChunk seperator, IEnumerable<Node> items)
        {
            List<TextChunk> result = new List<TextChunk>();
            bool itemsAdded = false;

            result.Add(leftBrace);
            foreach (Node arg in items)
            {
                itemsAdded = true;
                result.AddRange(arg.HighlightCode());
                result.Add(seperator);
            }
            if (true == itemsAdded)
            {
                result.RemoveAt(result.Count - 1);
            }
            result.Add(rightBrace);
            return result;
        }

        private static IEnumerable<TextChunk> Arg(Arg arg, int indent = 0)
            => arg.Expression.HighlightCode(indent);

        public static IEnumerable<TextChunk> CallIndexCode(IndexExpression expression, int indent = 0)
            => expression.Target.HighlightCode(indent);

        private static TextChunk Chunk(string value, TokenType tokenType = TokenType.DefClass)
            => new TextChunk(value, tokenType);
    }
}
