﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PythonRepl
{
    public class Command
    {
        public string Name { get; set; } = ":unknown";
        public List<string> AlternateNames { get; set; } = new List<string>();
        public List<string> SubCommands { get; set; } = new List<string>();
        public CommandType CommandType { get; set; }
        public Func<Repl, string, int> Action { get; set; } = (r, c) => {
            //d.Outputs.General.Lines.Add(new OutputLine($"Unknown command type {c}", ConsoleColor.Red));
            return 0;
        };
        /*
        public Func<CoreDebug, IEnumerable<string>> SubOptions { get; set; }
        public Func<CoreDebug, string,  Task<bool>> Action { get; set; } = (d, c) => {
            d.Outputs.General.Lines.Add(new OutputLine($"Unknown command type {c}", ConsoleColor.Red));
            return Task.FromResult<bool>(true);
        };
        public Func<CoreDebug, List<string>, List<string>> ResponsePreProcess { get; set; } = (d, r) => r;
        public Func<CoreDebug, string, string> PreProcess { get; set; } = (d, c) => c;
        */
        public bool IsInternal { get => Name.StartsWith(":"); }

        internal int Execute(Repl repl, string fullCommand)
        {
            int result = 0;
            if(default(Func<Repl, string, int>) != Action)
            {
                result = Action(repl, fullCommand);
            }
            return result;
        }
    }

    public enum CommandType
    {
        Info,
        Navigation
    }

    public class Commands
    {
        public Commands()
        {
            IEnumerable<char> alphabet = Enumerable.Range('a', 26).Select(x => (char)x);
        }
        public Command GetCommand(string command)
        {
            Command result = new Command();

            Match match = Regex.Match(command, @"^([a-zA-Z0-9_:]+)\s*");
            if(default(Match) != match && match.Success)
            {
                command = match.Groups[1].Value;
            }

            Command foundCommand = MainCommands.Find(c => 
                (command == c.Name) || c.AlternateNames.Contains(command)
            );

            if(default(Command) != foundCommand)
            {
                result = foundCommand;
            }
            return result;
        }

        public readonly List<Command> MainCommands = new List<Command>()
            {
                new Command() {
                    Name = ":scope",
                    AlternateNames = new List<string>() { "v" },
                    Action = (r, c) =>
                    {
                        foreach(ScopeInfo scopeInfo in r.Python.ScopeInfo())
                        {
                            ConsoleUtils.WriteLine(scopeInfo.Title, ConsoleColor.Yellow);
                            ConsoleUtils.WriteLine(scopeInfo.Members);
                            //Console.WriteLine(scopeInfo.Title);
                            //Console.WriteLine(scopeInfo.Members);
                        }
                        return 4;
                    }
                },
                new Command()
                {
                    Name = ":grep",
                    AlternateNames = new List<string>() { ":g" },
                    Action = (r, c) =>
                    {
                        string[] command = c.Split(' ');

                        /*
                        ShellProcess process = new ShellProcess();
                        List<string> lines = await process.Run(d.Settings.Get("ripgrepfilename"), $@"""(function|subroutine)\s+(?i){command[1]}"" .", d.State.SourceDirectory);

                        d.Outputs.General.Lines.AddRange(lines.Select(l => new OutputLine(l)));
                        */
                        return 0;
                    }
                },
                new Command() {
                    Name = ":save",
                    Action = (r, c) => 0//d.State.Save(d, c)
                },
                new Command() {
                    Name = ":load",
                    Action = (r, c) => 0//d.State.Load(d, c)
                },
                new Command() {
                    Name = ":history",
                    AlternateNames = new List<string>() { ":h"},
                    Action = (r, c) => { ReadLine.ClearHistory(); return 0; }
                },
                new Command() {
                    Name = ":quit",
                    AlternateNames = new List<string>() { ":q"},
                    Action = (r, c) => 0
                },
                new Command() {
                    Name = ":four",
                    AlternateNames = new List<string>() { ":f"},
                    Action = (r, c) => {
                        Console.WriteLine("Line 1");
                        Console.WriteLine("Line 2");
                        Console.WriteLine("Line 3");
                        Console.WriteLine("Line 4");
                        return 4;
                    }
                }
        };
    }
}
