﻿using Microsoft.Scripting.Hosting;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Runtime.Remoting;

namespace PythonRepl.AutoComplete
{
    class Completion
    {
        public Regex Regex { get; set; }
        public Func<Match, string, IEnumerable<string>> GetOptions { get; set; }

        public Completion(Regex regex, Func<Match, string, IEnumerable<string>> getOptions)
        {
            Regex = regex;
            GetOptions = getOptions;
        }
    }

    partial class AutoCompletionHandler : IAutoCompleteHandler
    {
        private static Commands _commands = new Commands();
        // characters to start completion from
        public char[] Separators { get; set; } = new char[] { ' ', '.', '/', '(', ',', '[', '"', ':' };
        private Repl Repl { get; set; }
        private readonly IEnumerable<string> _keywords = new List<string>() { "print", "dir", "len", "def" };


        private static Completion Completion(string regex, Func<Match, string, IEnumerable<string>> options)
            => new Completion(new Regex(regex), options);

        private static IEnumerable<string> _noSuggestions = new List<string>() { "No suggestions.." };

        private IEnumerable<Completion> _completions;

        public AutoCompletionHandler(Repl repl)
        {
            Repl = repl;
            _completions = new List<Completion>()
            {
                Completion(@"^from\s([a-zA-Z]?)", (m, t)
                    => GetNamespaces(m, t)),
                Completion(@"""([\/\w]+)", (m, t)
                    => GetPathOptions(m, t)),
                Completion(@":([a-zA-Z0-9_]*)$", (m, t)
                    => GetCommands(m, t)),

                Completion(@"([\w]+)\.([\w]+)\(lambda\s+([\w,\s]+):\s([\w]+)\.([\w]*)$", (m, t)
                    => GetLambdaOptions(m, t)),

                Completion(@"([\w]+)\.([\w]+)\s=\s([\w]+)\.([\w]*)", (m, t)
                    => GetAssignmentOptionsProperty(m, t)),
                Completion(@"([\w]+)\.([\w]+)\s=\s([\w]*)$", (m, t)
                    => GetAssignmentOptions(m, t)),

                Completion(@"\[([\w]*)$", (m, t)
                    => GetTypeOptions(m, t)),

                Completion(@"([\w]+)\.([\w]*)$", (m, t)
                    => GetPossibleOptions(GetParameters(m), m, t)),
                Completion(@"([\w]*)$", (m, t)
                    => GetPossibleOptions(Repl.Python.GetVariablesInfo(), m, t)),
            };
        }


        // ([\w]+)\.([\w]+)\s=\s([\w]*)$
        private IEnumerable<string> GetAssignmentOptions(Match match, string fullText)
        {
            IEnumerable<string> result = _noSuggestions;
            // 0 = full text
            // 1 = object
            // 2 = property
            // 3 = possible variable
            string targetObjectName         = match.Groups[1].Value;
            string targetPropertyName       = match.Groups[2].Value;
            string possibleVariable         = match.Groups[3].Value;

            var targetObject = Repl.Python.Scope.GetVariable(targetObjectName);
            PropertyInfo targetProperty = targetObject?.GetType()?.GetProperty(targetPropertyName);

            if(default(PropertyInfo) != targetProperty)
            {
                result = FilterOptions(GetVariables(), targetProperty.PropertyType, possibleVariable);
            }

            return result;
        }

        private IEnumerable<VariableInfo> FilterByType(IEnumerable<VariableInfo> options, Type type) =>
            options.Where(v => v.Type == type);

        private IEnumerable<VariableInfo> FilterOutBaseDataTypes(IEnumerable<VariableInfo> options)
        {
            IEnumerable<VariableInfo> result = options.Where(v =>
            {
                return typeof(Int32) != v.Type
                            && typeof(Decimal) != v.Type
                            && typeof(string) != v.Type;
            });

            return result;
        }

        private IEnumerable<VariableInfo> GetVariables() =>
            Repl.Python.GetVariablesInfo();
        
        private IEnumerable<string> FilterOptions(IEnumerable<VariableInfo> options, Type type, string startsWith) 
        {
            IEnumerable<VariableInfo> result = default(IEnumerable<VariableInfo>);
            IEnumerable<VariableInfo> matchedVariables = options;

            if(false == string.IsNullOrEmpty(startsWith))
            {
                matchedVariables = options.Where(v => v.Name.StartsWith(startsWith));
            }

            if(default(Type) != type)
            {
                result = FilterByType(matchedVariables, type);
            }

            result = result.Concat(FilterOutBaseDataTypes(matchedVariables));

            return result.Select(v => v.Name);
        }

        private IEnumerable<string> GetAssignmentOptionsProperty(Match match, string fullText)
        {
            // 0 = full text
            // 1 = object
            // 2 = property
            // 3 = object
            // 4 = possible property
            IEnumerable<string> result = _noSuggestions;

            string targetObjectName             = match.Groups[1].Value;
            string targetPropertyName           = match.Groups[2].Value;
            string sourceObjectName             = match.Groups[3].Value;
            string possibleSourcePropertyName   = match.Groups[4].Value;

            var targetObject = Repl.Python.Scope.GetVariable(targetObjectName);
            var targetProperty = targetObject?.GetType()?.GetProperty(targetPropertyName);
            var sourceObject = Repl.Python.Scope.GetVariable(sourceObjectName); 

            if (default(PropertyInfo) != targetProperty)
            {
                result = FilterOptions(GetProperties(sourceObject), targetProperty?.PropertyType, possibleSourcePropertyName);
            }
            return result;
        }

        private IEnumerable<string> GetTypeOptions(Match match, string fullText)
        {
            return GetPossibleOptions(Repl.Python.GetVariablesInfo(), match, fullText);
        }

        private IEnumerable<string> GetLambdaOptions(Match match, string fullText)
        {
            // 0 = full text
            // 1 = object
            // 2 = method
            // 3 = params
            // 4 = paramused
            // 5 = method of param

            return GetPossibleOptions(Repl.Python.GetVariablesInfo(), match, fullText);
        }

        private static IEnumerable<VariableInfo> GetProperties(object variable)
        {
            List<VariableInfo> result = new List<VariableInfo>();
            Type variableType = (Type)variable.GetType();

            PropertyInfo[] properties = variableType.GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);
            result.AddRange(properties.Select(propertyInfo => new VariableInfo(propertyInfo.Name, propertyInfo.PropertyType))
                                        .ToList()
                                        .Distinct());

            return result;
        }

        private static IEnumerable<VariableInfo> GetMethods(object variable)
        {
            List<VariableInfo> result = new List<VariableInfo>();
            Type variableType = (Type)variable.GetType();

            var methods = variableType.GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly)
                .Where(methodInfo => !(methodInfo.Name.StartsWith("get_") || methodInfo.Name.StartsWith("set_")));

            result.AddRange(methods.Select(methodInfo => new VariableInfo(methodInfo.Name, methodInfo.ReturnType)));
            result.AddRange(GetExtensionMethods(null, variableType).Select(min => new VariableInfo(min.Name, min.ReturnType)));

            return result;
        }

        private static IEnumerable<string> GetPossibleOptions(IEnumerable<VariableInfo> possibleOptions, Match match, string context)
        {
            IEnumerable<string> result = new List<string>();
            IEnumerable<VariableInfo> distinctOptions = possibleOptions.Distinct();

            if(default(Match) != match)
            {
                string matchText = match.Groups[match.Groups.Count - 1].Value;
                result = distinctOptions.Where(o => true == o.Name.StartsWith(matchText)).Select(o => o.Name);
            }
            else
            {
                result = distinctOptions.Select(o => o.Name);
            }

            return result;
        }


        #region get methods
        private static IEnumerable<MethodInfo> GetExtensionMethods(Assembly assembly, Type extendedType)
        {
            List<MethodInfo> extension_methods = new List<MethodInfo>();
            if (typeof(IEnumerable<object>).IsAssignableFrom(extendedType))
            {
                Type t = typeof(Enumerable);

                foreach (MethodInfo mi in t.GetMethods().Distinct())
                {
                    if (mi.IsDefined(typeof(ExtensionAttribute), false))
                    {
                        extension_methods.Add(mi);
                    }
                }

            }

            return extension_methods;
        }

        private IEnumerable<VariableInfo> GetParameters(Match match)
        {
            List<VariableInfo> result = new List<VariableInfo>();
            string variableName = match.Groups[1].Value;
            string param = match.Groups[2].Value;

            try
            {
                var variable = Repl.Python.Scope.GetVariable(variableName);

                if (null != variable)
                {
                    Type variableType = (Type)variable.GetType();

                    MethodInfo[] methodInfo = variableType.GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);

                    var methods = methodInfo.Where(m => !(m.Name.StartsWith("get_") || m.Name.StartsWith("set_")))
                                            .Select(mi => new VariableInfo(mi.Name, mi.ReturnType))
                                            .ToList();
                    result.AddRange(methods);


                    result.AddRange(GetExtensionMethods(null, variableType).Select(min => new VariableInfo(min.Name, min.ReturnType)));

                    PropertyInfo[] propertyInfo = variableType.GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);

                    result.AddRange(propertyInfo.Select(pi => new VariableInfo(pi.Name, pi.PropertyType)).ToList().Distinct());
                }
            }
            catch (Exception e)
            {

            }

            return result;
        }
        #endregion

        // text - The current text entered in the console
        // index - The index of the terminal cursor within {text}
        public string[] GetSuggestions(string text, int index)
        {
            List<string> result = new List<string>();

            //ConsoleUtils.WriteInfoLine($"AC {text}");

            List<string> options = new List<string>();
            foreach (Completion completion in _completions)
            {
                Match match = completion.Regex.Match(text);
                if (default(Match) != match)
                {
                    if (true == match.Success)
                    {
                        result.AddRange(completion.GetOptions(match, text));
                        break;
                    }
                }
            }

            return result.ToArray();
        }

        #region namespaces
        private IEnumerable<string> GetNamespaces(Match m, string t)
        {
            string start = t.Substring(4).Trim();
            int dotPos = start.LastIndexOf('.');
            IEnumerable<string> result = Repl.Python.Namespaces.Where(n => n.StartsWith(start))
                                                  .Select(n => Next(n))
                                                  .Distinct()
                                                  .ToList();

            string Next(string @namespace)
            {
                string next = @namespace;
                if (-1 != dotPos)
                {
                    next = @namespace.Substring(dotPos + 1);
                }

                if (-1 != next.IndexOf("."))
                {
                    next = next.Substring(0, next.IndexOf("."));
                }

                return next;
            }
            return result;
        }
        #endregion
    }
}
