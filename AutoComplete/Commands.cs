﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace PythonRepl.AutoComplete
{
    partial class AutoCompletionHandler
    {
        private IEnumerable<string> GetCommands(Match match, string t)
        {
            string startsWith = match.Groups[1].Value;
            //return _commands.MainCommands.Where(c => c.Name.StartsWith(startsWith)).Select(c => c.Name.Substring(1));
            return _commands.MainCommands.Select(c => c.Name.Substring(1));
        }
    }
}
