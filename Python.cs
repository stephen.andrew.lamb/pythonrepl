﻿using IronPython.Hosting;
using IronPython.Compiler;
using Microsoft.Scripting.Hosting;
using Microsoft.Scripting.Hosting.Providers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Scripting;
using Microsoft.Scripting.Runtime;
using IronPython;
using IronPython.Compiler.Ast;
using System.Runtime.Remoting;
using Microsoft.Scripting.Interpreter;

namespace PythonRepl
{
    public class PythonUtils
    {
        private List<VariableInfo> TypeNames { get;  set; }           = new List<VariableInfo>();
        public List<VariableInfo> Enums { get; set; }                 = new List<VariableInfo>();
        public List<VariableInfo> Attributes { get; set; }            = new List<VariableInfo>();
        public List<VariableInfo> Interfaces { get; set; }            = new List<VariableInfo>();
        public List<VariableInfo> Classes { get; set; }               = new List<VariableInfo>();
        public List<string> Namespaces { get; set; }                  = new List<string>();

        public IEnumerable<VariableInfo> All { get; set; }            = new List<VariableInfo>();

        public IEnumerable<VariableInfo> Variables { get; set; } = new List<VariableInfo>();

        public Exception LastError { get; private set; }

        public ScriptEngine Engine { get; private set; }
        public ScriptScope Scope { get; private set; }

        private LanguageContext _languageContext;

        public PythonUtils()
        {
            Engine = IronPython.Hosting.Python.CreateEngine();
            Scope = Engine.CreateScope();

            _languageContext = HostingHelpers.GetLanguageContext(Engine);

            Scope.ImportModule("clr");
            Scope.ImportModule("sys");
            Engine.Execute("import clr");
            Engine.Execute("import sys");
            string setPath = $"sys.path.append(\"{ Environment.GetEnvironmentVariable("PYTHONPATH")}\")";
            RunCode(setPath);

            Engine.Execute("clr.AddReference(\"System\")", Scope);
            Engine.Execute("clr.AddReference(\"System.Core\")", Scope);

            Engine.Execute("import System", Scope);
            Engine.Execute("from System import Linq", Scope);
            Engine.Execute("clr.ImportExtensions(System.Linq)", Scope);
        }

        public void AddVariable(string name, object variable)
        {
            Scope.SetVariable(name, variable);
        }

        private void GetTypes(IEnumerable<VariableInfo> types)
        {
            IEnumerable<VariableInfo> typeNames = types;

            IEnumerable<VariableInfo> enums = types.Where(i => i.Type.BaseType?.FullName == "System.Enum");

            IEnumerable<VariableInfo> attributes = types.Where(i => i.Type.BaseType?.FullName == "System.Attribute");


            IEnumerable<VariableInfo> interfaces = types.Where(i => default(Type) == i.Type.BaseType);

            IEnumerable<VariableInfo> classes = typeNames.Except(enums)
                                                         .Except(interfaces)
                                                         .Except(attributes);

            IEnumerable<string> namespaces = types
                                             .Select(t => t.Type.Namespace)
                                             .Where(n => default(string) != n)
                                             .Distinct();

            TypeNames.AddRange(
                typeNames.Except(TypeNames)
                         .Except(attributes)
                         .Except(interfaces)
                         .Except(enums)
            );
            Enums.AddRange(enums.Except(Enums));
            Attributes.AddRange(attributes.Except(Attributes));
            Interfaces.AddRange(interfaces.Except(Interfaces));
            Classes.AddRange(classes.Except(Classes));
            Namespaces.AddRange(namespaces.Except(Namespaces));

            All = All.Union(TypeNames)
                     .Union(Enums)
                     .Union(Attributes)
                     .Union(Interfaces)
                     .Union(Classes);
        }

        public void AddAssembly(string fileName, string tag = "", string importNamespace = "")
        {
            string fullFileName = Path.Combine(Directory.GetCurrentDirectory(), fileName);
            Assembly assembly = Assembly.Load(fileName);
            IEnumerable<VariableInfo> types = assembly.GetTypes()
                                              .Where(t => default(Type) != t)
                                              .Select(t => new VariableInfo(t.Name, t, tag));

            GetTypes(types);

            RunCode($"clr.AddReference(\"{fileName}\")");

            if (false == string.IsNullOrEmpty(importNamespace))
            {
                RunCode($"from {importNamespace} import *");
            }
        }

        public static Func<PythonUtils, IEnumerable<VariableInfo>> EnumsScope       = p => p.Enums;
        public static Func<PythonUtils, IEnumerable<VariableInfo>> AttributesScope  = p => p.Attributes;
        public static Func<PythonUtils, IEnumerable<VariableInfo>> EnumsScopeScope  = p => p.Enums;

        public Dictionary<string, Func<PythonUtils, IEnumerable<VariableInfo>>> ScopeSections
            = new Dictionary<string, Func<PythonUtils, IEnumerable<VariableInfo>>>()
            {
                { "Function",   p => p.GetVariablesInfo().Where(v => v.TypeGroup == TypeGroup.Function) },
                { "Variable",   p => p.GetVariablesInfo().Where(v => v.TypeGroup == TypeGroup.Variable) },
                { "Buisness Logic",   p => p.GetVariablesInfo().Where(v => v.TypeGroup == TypeGroup.BuisnessLogic) },
                //{ "All",        p => p.All },
                //{ "All Scope",  p => p.GetVariablesInfo() },
            };

        public IEnumerable<ScopeInfo> ScopeInfo()
        {
            IEnumerable<VariableInfo> variables = GetVariablesInfo();

            List<ScopeInfo> result = new List<ScopeInfo>();

            foreach(KeyValuePair<string, Func<PythonUtils, IEnumerable<VariableInfo>>> section in ScopeSections)
            {
                IEnumerable<VariableInfo> items = section.Value(this);
                if(true == items.Any())
                {
                    result.Add(
                        new ScopeInfo()
                        {
                            Title = section.Key,
                            Members = string.Join(", ", items.Select(v => v.Name))
                        }
                    );
                }
            }
            return result;
        }

        public IEnumerable<VariableInfo> GetVariablesInfo()
        {
            IEnumerable<VariableInfo> result = GetVariablesNames().Select(v => new VariableInfo(this, v));
            //IEnumerable<VariableInfo> result = Scope.GetVariableNames().Select(v => new VariableInfo(this, v));

            return result;
        }

        private IEnumerable<string> GetVariablesNames()
            => Scope.GetVariableNames();

        #region unused
        private VariableInfo GetVariableInfo(string variableName)
            => new VariableInfo(this, variableName);
        private IEnumerable<dynamic> GetVariables()
            => Scope.GetVariableNames().Select(vn => Scope.GetVariable(vn)).ToList();
        #endregion

        public void Execute(string scriptFile)
        {
            ScriptSource source = Engine.CreateScriptSourceFromFile(scriptFile);
            RunCode(source, "");
        }


        public void ExecuteFile(string fullPath)
        {
            ScriptSource source = Engine.CreateScriptSourceFromFile(fullPath);
            RunCode(source, "");
        }

        public dynamic RunCode(string code)
        {
            ScriptSource source = Engine.CreateScriptSourceFromString(code);
            dynamic result = RunCode(source, code);
            return result;
        }

        /*
        private List<string> _variables = new List<string>();
        public List<string> Variables { get => _variables; }
        public void CheckVariables()
        {
            IEnumerable<string> currentVariables = GetVariablesNames();
            _variables.AddRange(currentVariables.Except(_exclusions));
            _variables = _variables.Distinct().ToList();

        }
        private IEnumerable<string> _exclusions;
        public void InitVariables(List<string> allowed)
        {
            _exclusions = GetVariablesNames().Except(allowed);
        }
        */
        public dynamic RunCode(ScriptSource source, string code)
        {
            dynamic result = new { };
            try
            {
                result = source.Execute(Scope);

                if (null != result)
                {
                    source = Engine.CreateScriptSourceFromString("output(" + code + ")");
                    source.Execute(Scope);
                }
                //CheckVariables();
            }
            catch (IronPython.Runtime.UnboundNameException e)
            {
                ConsoleUtils.WriteLine(e.Message, ConsoleColor.Red);
                LastError = e;
            }
            catch (Exception e)
            {
                ConsoleUtils.WriteLine(e.Message, ConsoleColor.Red);
                LastError = e;
            }
            return result;
        }

        public PythonAst GetAst(string code, AstParseMode mode)
        {
            PythonAst ast = default(PythonAst);
            var source = HostingHelpers.GetSourceUnit(Engine.CreateScriptSourceFromString(code));

            Parser parser = Parser.CreateParser(
                new CompilerContext(source, _languageContext.GetCompilerOptions(), ErrorSink.Default), 
                (PythonOptions)_languageContext.Options
            );

            ScriptCodeParseResult scriptCodeParseResult;
            try
            {
                if(AstParseMode.Interactive == mode)
                {
                    ast = parser.ParseInteractiveCode(out scriptCodeParseResult);
                }
                if(AstParseMode.File == mode)
                {
                    ast = parser.ParseFile(false);
                }
            } 
            catch (Exception e)
            {
                ConsoleUtils.WriteLine(e.Message, ConsoleColor.Red);
            }
            

            return ast;

        }
    }

    #region VariableInfo
    public enum TypeGroup
    {
        Type, Variable, Function, Model, BuisnessLogic, Unknown
    }

    public class VariableInfo : IEquatable<VariableInfo>
    {
        public string Name { get; private set; }
        public string Tag { get; private set; }
        public Type Type { get => GetTypeInfo(); }
        public TypeGroup TypeGroup { get => GetTypeGroup();  }
        private PythonUtils _python { get; }

        public VariableInfo(PythonUtils python, string name, string tag = "")
        {
            _python = python;
            Name = name;
            Tag = tag;
        }

        public VariableInfo(string name, Type type, string tag = "")
        {
            _type = type;
            Name = name;
            Tag = tag;
        }

        private Type _type = default(Type);
        private object _obj = default(Object);
        private Type GetTypeInfo()
        {
            if (default(Type) == _type)
            {
                try
                {
                    ObjectHandle varHandle = _python.Scope.GetVariableHandle(Name);
                    _obj = varHandle.Unwrap();

                    if (default(object) != _obj)
                    {
                        _type = _obj?.GetType();
                    }
                }
                catch (Exception e)
                {

                }
            }
            return _type;
        }
        
        public static Dictionary<string, TypeGroup> TypeGroups = new Dictionary<string, TypeGroup>()
        {
            { "System", TypeGroup.Variable },
            { "IronPython.Runtime.PythonFunction", TypeGroup.Function },
            { "IronPython.Runtime.Types.PythonType", TypeGroup.Type }
        };

        private TypeGroup GetTypeGroup()
        {
            TypeGroup result = TypeGroup.Unknown;
            Type type = Type;
            if(default(Type) != type)
            {
                foreach (KeyValuePair<string, TypeGroup> typeGroup in TypeGroups)
                {
                    if (type.FullName.StartsWith(typeGroup.Key))
                    {
                        result = typeGroup.Value;
                        break;
                    }
                }
            }
            return result;
        }

        #region standardstuff
        public override bool Equals(object obj)
        {
            return Equals(obj as VariableInfo);
        }

        public bool Equals(VariableInfo other)
        {
            return other != null &&
                   Name == other.Name &&
                   EqualityComparer<Type>.Default.Equals(Type, other.Type);
        }

        public override int GetHashCode()
        {
            var hashCode = -243844509;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Name);
            hashCode = hashCode * -1521134295 + EqualityComparer<Type>.Default.GetHashCode(Type);
            return hashCode;
        }
        #endregion
    }
    #endregion

    public class ScopeInfo
    {
        public string Title { get; set; }
        public string Members { get; set; }
    }


    public enum AstParseMode
    {
        Interactive, File
    }
}
