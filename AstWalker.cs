﻿using IronPython.Hosting;
using IronPython.Compiler;
using Microsoft.Scripting.Hosting;
using Microsoft.Scripting.Hosting.Providers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Scripting;
using Microsoft.Scripting.Runtime;
using IronPython;
using IronPython.Compiler.Ast;

namespace PythonRepl
{
    public class AstWalker: PythonWalker
    {
        public AstWalker()
        {
        }

        public string Result { get; private set; }
        public List<Node> Nodes = new List<Node>();

        public string Code()
        {
            string result = "";
            //Nodes.OrderBy<Node>(n)
            return result;
        }

        protected void AddToOutput(string value, TokenType tokenType)
        {
            Result = Result + value;
        }

        public override bool Walk(AssignmentStatement node)
        {
            Nodes.Add(node);
            return base.Walk(node); 
        }
        public override bool Walk(NameExpression node)
        {
            Nodes.Add(node);
            return base.Walk(node);
        }
        public override bool Walk(CallExpression node)
        {
            Nodes.Add(node);
            return base.Walk(node);
        }
    }
}
