﻿using System;
using System.Collections.Generic;
using System.Linq;
using IronPython.Compiler;
using IronPython.Compiler.Ast;
using System.Linq.Expressions;
using LinqExpression = System.Linq.Expressions.Expression;

using PythonExpression          = IronPython.Compiler.Ast.Expression;
using PythonConstantExpression  = IronPython.Compiler.Ast.ConstantExpression;
using PythonBinaryExpression    = IronPython.Compiler.Ast.BinaryExpression;
using PythonUnaryExpression     = IronPython.Compiler.Ast.UnaryExpression;
using PythonLambdaExpression    = IronPython.Compiler.Ast.LambdaExpression;
using PythonMemberExpression    = IronPython.Compiler.Ast.MemberExpression;
using PythonRepl.Python.SyntaxHighlight;

namespace PythonRepl
{
    public class LinqExpressionGeneratorException: Exception{
        public PythonExpression Expression { get; private set; }
        public LinqExpressionGeneratorException(string message, PythonExpression expression)
            : base(message)
        {
            Expression = expression;
        }
    }

    /// <summary>
    /// Generate Expression trees from python ast
    /// </summary>
    public static class AstNodeExpressionGenerators
    {
        public static Dictionary<string, FunctionDefinition> Functions = new Dictionary<string, FunctionDefinition>();

        private static Dictionary<string, ParameterExpression> _parameters = new Dictionary<string, ParameterExpression>();

        private static Dictionary<PythonOperator, Func<LinqExpression, LinqExpression, LinqExpression>> _operators
            = new Dictionary<PythonOperator, Func<LinqExpression, LinqExpression, LinqExpression>>()
        {
            { PythonOperator.Equals,                (l, r) => LinqExpression.Equal(l, r) },
            { PythonOperator.NotEqual,              (l, r) => LinqExpression.NotEqual(l, r) },
            { PythonOperator.GreaterThan,           (l, r) => LinqExpression.GreaterThan(l, r) },
            { PythonOperator.GreaterThanOrEqual,    (l, r) => LinqExpression.GreaterThanOrEqual(l, r) },
            { PythonOperator.LessThan,              (l, r) => LinqExpression.LessThan(l, r) },
            { PythonOperator.LessThanOrEqual,       (l, r) => LinqExpression.LessThanOrEqual(l, r) },
            { PythonOperator.Multiply,              (l, r) => LinqExpression.Multiply(l, r) },
            { PythonOperator.Subtract,              (l, r) => LinqExpression.Subtract(l, r) },
            { PythonOperator.Add,                   (l, r) => LinqExpression.Add(l, r) },
            { PythonOperator.Divide,                (l, r) => LinqExpression.Divide(l, r) },
        };

        private static Dictionary<PythonOperator, Func<LinqExpression, LinqExpression>> _unaryOperators
            = new Dictionary<PythonOperator, Func<LinqExpression,LinqExpression>>()
        {
            { PythonOperator.Not,                   (e) => LinqExpression.Not(e) },
        };

        private static Dictionary<string, Func<object, LinqExpression>> _nodeTypes
            = new Dictionary<string, Func<object, LinqExpression>>()
        {
            { "SuiteStatement",         (e) => SuiteStatementCode       (e as SuiteStatement) },
            { "IfStatement",            (e) => IfStatementCode          (e as IfStatement) },
            { "ReturnStatement",        (e) => ReturnStatement          (e as ReturnStatement) },

            { "NameExpression",         (e) => NameExpressionCode       (e as NameExpression) },
            { "ConstantExpression",     (e) => ConstantExpression       (e as PythonConstantExpression) },
            { "BinaryExpression",       (e) => BinaryExpression         (e as PythonBinaryExpression) },
            { "ParenthesisExpression",  (e) => ParenthesisExpression    (e as ParenthesisExpression) },
            { "LambdaExpression",       (e) => LambdaExpressionCode     (e as PythonLambdaExpression) },

            { "MemberExpression",       (e) => MemberExpressionCode     (e as PythonMemberExpression) },

            { "AndExpression",          (e) => AndExpression            (e as AndExpression) },
            { "OrExpression",           (e) => OrExpression             (e as OrExpression) },
            { "UnaryExpression",        (e) => UnaryExpression          (e as PythonUnaryExpression) },
        };

        public static PythonUtils Python { get; internal set; }

        private static LinqExpression GenerateCode(this PythonExpression expression) => RunCode(expression);
        private static LinqExpression GenerateCode(this LinqExpression expression) => RunCode(expression);
        private static LinqExpression RunCode(object expression)
        {
            List<TextChunk> result = new List<TextChunk>();
            string nodeType = expression.GetType().ToString().Substring(24);
            if (_nodeTypes.ContainsKey(nodeType))
            {
                return _nodeTypes[nodeType](expression);
            }
            else
            {
                throw new LinqExpressionGeneratorException($"Cannot find nodetype '{nodeType}'", (PythonExpression)expression);
            }
        }


        public static LinqExpression CreateExpressionFromFunction<T>(string functionName)
        {
            FunctionDefinition function = Functions[functionName];
            IEnumerable<ParameterExpression> parameters = function.Parameters.Select(p => LinqExpression.Parameter(typeof(T), p.Name));

            foreach(ParameterExpression parameter in parameters)
            {
                _parameters[parameter.Name] = parameter;
            }

            var result  =  LinqExpression.Lambda<Func<T, bool>>(function.Body.GenerateCode(), parameters);

            return result;
        }

        private static LinqExpression BinaryExpression(PythonBinaryExpression binaryExpression)
        {
            // check compatability 
            return Operator(binaryExpression.Operator, binaryExpression.Left, binaryExpression.Right);
        }
            
        private static LinqExpression AndExpression(AndExpression andExpression)
            => LinqExpression.And(GenerateCode(andExpression.Left), GenerateCode(andExpression.Right));

        private static LinqExpression OrExpression(OrExpression orExpression)
            => LinqExpression.Or(GenerateCode(orExpression.Left), GenerateCode(orExpression.Right));

        private static LinqExpression UnaryExpression(PythonUnaryExpression unaryExpression)
            => UnaryOperator(unaryExpression.Op, unaryExpression.Expression);

        private static LinqExpression ConstantExpression(PythonConstantExpression constantExpression)
            => LinqExpression.Constant(constantExpression.Value, constantExpression.Type);

        private static LinqExpression ParenthesisExpression(ParenthesisExpression parenthesisExpression)
            => parenthesisExpression.Expression.GenerateCode();

        private static LinqExpression ReturnStatement(ReturnStatement returnStatement)
            => returnStatement.Expression.GenerateCode();

        private static LinqExpression Operator(PythonOperator @operator, PythonExpression left, PythonExpression right)
            => _operators[@operator](GenerateCode(left), GenerateCode(right));

        private static LinqExpression UnaryOperator(PythonOperator @operator, PythonExpression expression)
            => _unaryOperators[@operator](expression.GenerateCode());



        private static LinqExpression SuiteStatementCode(SuiteStatement suiteStatement)
        {
            /*
            List<TextChunk> result = new List<TextChunk>();

            foreach (Statement item in suiteStatement.Statements)
            {
                result.AddRange(item.GenerateCode(indent));
                result.Add(Chunk("\n"));
            }
            return result;
            */
            var statement = suiteStatement.Statements.FirstOrDefault();

            return statement.GenerateCode();
        }

        private static LinqExpression NameExpressionCode(NameExpression expression)
        {
            if (_parameters.ContainsKey(expression.Name))
            {
                ParameterExpression parameter = _parameters[expression.Name];
                return parameter;
            }
            if (Python.Scope.ContainsVariable(expression.Name))
            {
                var getVariable = typeof(PythonUtils).GetMethod(nameof(PythonUtils.Scope.GetVariable));
                //var getVariable = typeof(PythonUtils).GetMethods().Where(m => m.Name == "GetVariable").FirstOrDefault();


                //var variableName = LinqExpression.Constant(expression.Name);
                //var var1 = LinqExpression.Variable(typeof(object), "v");
                //LinqExpression.Assign(var1, LinqExpression.Call(LinqExpression.Constant(Python), getVariable, variableName));
                //LinqExpression.Call(LinqExpression.Constant(Python), getVariable, variableName));

                //var test = LinqExpression.Call(Scope, "GetVariable", null, variableName);
                
                //var result = LinqExpression.Call(LinqExpression.Constant(Python), getVariable, variableName);
                var variable = Python.Scope.GetVariable(expression.Name);
                var result = LinqExpression.Constant(variable);
                return result;
            }
            if ("True" == expression.Name)
            {
                return LinqExpression.Constant(true);
            }
            if ("False" == expression.Name)
            {
                return LinqExpression.Constant(false);
            }

            throw new LinqExpressionGeneratorException($"Cannot find parameter or in scope variable {expression.Name}", expression);
        }

        private static LinqExpression MemberExpressionCode(PythonMemberExpression expression)
        {
            var target = GenerateCode(expression.Target);
            var result = LinqExpression.PropertyOrField(target, expression.Name);

            if(default(LinqExpression) != target && default(LinqExpression) != result)
            {
                return result;
            }
            else
            {
                throw new LinqExpressionGeneratorException($"Cannot find parameter {expression.Name}", expression);
            }
        }

        private static LinqExpression LambdaExpressionCode(PythonLambdaExpression lambdaExpression)
        {
            /*
            List<TextChunk> result = new List<TextChunk>();
            bool itemsAdded = false;

            result.Add(Chunk($"lambda ", TokenType.Method));
            foreach (Parameter arg in lambdaExpression.Function.Parameters)
            {
                itemsAdded = true;
                result.AddRange(arg.GenerateCode());
                result.Add(Chunk(", ", TokenType.Operator));
            }
            if (true == itemsAdded)
            {
                result.RemoveAt(result.Count - 1);
            }

            result.Add(Chunk(": ", TokenType.Operator));
            result.AddRange(lambdaExpression.Function.Body.GenerateCode());

            return result;
            */
            throw new NotImplementedException();
        }

        private static LinqExpression IfStatementCode(IfStatement ifStatement)
        {
            /*
            List<TextChunk> result = new List<TextChunk>();
            bool itemsAdded = false;

            result.Add(Chunk($"{Indent(indent)}if ", TokenType.Method));
            result.AddRange(ifStatement.Tests.FirstOrDefault()?.Test.GenerateCode());
            result.Add(Chunk(":", TokenType.Operator));
            result.Add(Chunk("\n"));
            result.AddRange(ifStatement.Tests.FirstOrDefault()?.Body.GenerateCode(indent + 1));
            return result;
            */
            throw new NotImplementedException();
        }
    }
}
