﻿using IronPython.Runtime;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace PythonRepl
{
    public class OutputUtils
    {
        public bool ShowEmptyProperties { get; set; }
        public bool EnablePaging { get; set; } = false;

        public void Output(object obj)
        {
            Type variableType = (Type)obj.GetType();

            if ("String" == variableType.Name)
            {
                Console.WriteLine(obj.ToString());
                if (true == EnablePaging)
                {
                    Console.SetCursorPosition(0, Console.CursorTop + 1);
                }

                return;
            }

            if (variableType.Name == "PythonDictionary")
            {
                OutputDict(obj as PythonDictionary);
                if (true == EnablePaging)
                {
                    Console.SetCursorPosition(0, Console.CursorTop + 1);
                }

                return;
            }

            if (typeof(IEnumerable<object>).IsAssignableFrom(variableType))
            {
                OutputList(obj as IEnumerable<object>);
                if (true == EnablePaging)
                {
                    Console.SetCursorPosition(0, Console.CursorTop + 1);
                }

                return;
            }

            OutputObject(obj);

            if (true == EnablePaging)
            {
                Console.SetCursorPosition(0, Console.CursorTop + 1);
            }
        }

        public void OutputList(IEnumerable<object> list)
        {
            foreach (object item in list)
            {
                Output(item);
            }
        }

        public void OutputDict(PythonDictionary dictionary)
        {
            List<string> output = new List<string>();
            foreach (object key in dictionary.Keys)
            {
                output.Add($"{key} = {dictionary[key]}");
            }
            output.Add("");

            WritePaged(output);
        }

        public void OutputObject(object obj)
        {
            List<string> output = new List<string>();

            Type variableType = (Type)obj.GetType();

            PropertyInfo[] propertyInfo = variableType.GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);

            if (propertyInfo.Any())
            {
                bool propertyWritten = false;
                int width = propertyInfo.Max(p => p.Name.Length);
                foreach (PropertyInfo property in propertyInfo)
                {
                    try
                    {
                        object value = property.GetValue(obj);
                        if (null != value && (false == string.IsNullOrWhiteSpace(value.ToString())))
                        {
                            output.Add($"{property.Name.PadRight(width, ' ')} = {value}");
                            propertyWritten = true;
                        }
                    }
                    catch (Exception e)
                    {
                        output.Add(e.Message);
                    }
                }

                if (false == propertyWritten)
                {
                    output.Add("No properties set.");
                    output.Add(obj.ToString());
                }
                output.Add("");
            }
            else
            {
                output.Add(obj.ToString());
            }

            WritePaged(output);
        }
        public int WriteTable(List<List<string>> table)
        {
            List<string> output = new List<string>();
            List<int> widths = new List<int>();

            int columns = table.Max(l => l.Count);
            for (int i = 0; i < columns; i++)
            {
                try
                {
                    int width = table.Max(l => {
                        int length = 10;
                        if(i < l.Count)
                        {
                            length = l[i].Length;
                        }
                        
                        return length;
                    }) + 2;
                    widths.Add(width);
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Exception {e.Message}");
                }
            }

            foreach (List<string> line in table)
            {
                StringBuilder outputLine = new StringBuilder();
                int columnIndex = 0;
                foreach (string column in line)
                {
                    try
                    {
                        outputLine.Append(column.PadRight(widths[columnIndex]));
                        columnIndex++;
                    }
                    catch (Exception e)
                    {

                    }
                }
                output.Add(outputLine.ToString());
            }

            return WritePaged(output);
        }

        private int WritePaged(List<string> output)
        {
            int linesWritten = 0;
            if (true == EnablePaging)
            {
                int lines = 0;
                int maxLines = Console.WindowHeight - 2;
                foreach (string line in output)
                {
                    linesWritten++;
                    Console.WriteLine(line);
                    lines++;

                    if (maxLines == lines)
                    {
                        Console.WriteLine("Press return to continue...");
                        ConsoleKeyInfo key = Console.ReadKey();
                        if ('q' == key.KeyChar)
                        {
                            break;
                        }
                        lines = 0;
                    }
                }
            }
            else
            {
                foreach (string line in output)
                {
                    linesWritten++;
                    Console.WriteLine(line);
                }
            }
            //Console.WriteLine("");
            return linesWritten;
        }
    }
}
