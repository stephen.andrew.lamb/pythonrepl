﻿using System;
namespace PythonRepl
{
    public class ConsoleUtils
    {
        public ConsoleUtils()
        {

        }

        public static void Write(string message, ConsoleColor color = ConsoleColor.White)
        {
            Console.ForegroundColor = color;
            Console.Write(message);
            Console.ResetColor();
        }

        public static void WriteLine(string message, ConsoleColor color = ConsoleColor.White)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(message, color);
            Console.ResetColor();
        }

        public static void WriteInfoLine(string message)
        {
            int currentTop = Console.CursorTop;
            int currentLeft = Console.CursorLeft;
            Console.CursorTop = Console.WindowHeight;
            Console.CursorLeft = 0;
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Red;
            Console.Write(message);
            Console.BackgroundColor = ConsoleColor.Black;
            Console.CursorTop = currentTop;
            Console.CursorLeft = currentLeft;
        }
    }
}
