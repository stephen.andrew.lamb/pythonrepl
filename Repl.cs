﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.IO;
using Microsoft.Scripting.Hosting;
using System.Linq;
using IronPython.Runtime;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Internal.ReadLine.Abstractions;
using IronPython.Compiler.Ast;
using LinqExpression = System.Linq.Expressions.Expression;
using Optional;
using PythonRepl.AutoComplete;
using PythonRepl.Python.SyntaxHighlight;

namespace PythonRepl
{
    public class Repl
    {
        public PythonUtils Python;
        private SyntaxHighlight _syntaxHylighter;
        private Commands _commands = new Commands();
        public OutputUtils Output = new OutputUtils();

        public Func<PropertyInfo, List<string>> FieldInfo = pi => default(List<string>);

        public bool PreserveHistory { get; set; } = true;

        public Repl()
        {
            InitPython();
        }

        private void InitPython()
        {
            Python = new PythonUtils();
            Python.AddVariable("repl", this);

            ReadLine.HistoryEnabled = true;
            ReadLine.AutoCompletionHandler = new AutoCompletionHandler(this);
            /*
            ReadLine.Output = text =>
            {
                //PythonAst ast = Python.GetAst(text);
                //AstWalker walker = new AstWalker();
                //ast.Walk(walker);
                //string result = walker.Result;

                List<Token> tokens = _syntaxHylighter.Tokenize(text);
                _syntaxHylighter.WriteToConsole(tokens);
            };
            */
            Token.Scope = Python.Scope;

            _syntaxHylighter = new SyntaxHighlight();

            Python.RunCode("import sys");
            Python.RunCode(@"sys.path.append('C:\Program Files\python\Lib')");
            Python.RunCode(@"
def output(value):
    repl.Output.Output(value)
            ");

            Python.RunCode(@"
def table(value):
    repl.Table(value)
            ");

            Python.RunCode(@"
def info(value, alpha = False):
    repl.Info(value, alpha)
            ");

            Python.RunCode(@"
def save(path):
    repl.Save(path)
            ");

            Python.RunCode(@"
def load(path):
    repl.Load(path)
            ");
        }

        public void ExecuteFile(string fullPath)
        {
            Python.ExecuteFile(fullPath);
            string code = File.ReadAllText(fullPath);
            var ast = Python.GetAst(code, AstParseMode.File);
            ast.Body.HighlightCode();
        }

        #region history
        private void LoadHistory()
        {
            string historyFile = Path.Combine(Directory.GetCurrentDirectory(), ".repl_history");

            if (true == PreserveHistory)
            {
                if (File.Exists(historyFile))
                {
                    string[] history = File.ReadAllLines(historyFile);
                    ReadLine.AddHistory(history);
                }
            }
        }

        private void SaveHistory()
        {
            string historyFile = Path.Combine(Directory.GetCurrentDirectory(), ".repl_history");
            string[] history = default(string[]);

            if (true == PreserveHistory)
            {
                if (File.Exists(historyFile))
                {
                    File.Delete(historyFile);
                }

                history = ReadLine.GetHistory().Where(l => l != ":q").ToArray();

                File.WriteAllLines(historyFile, history);
            }
        }
        #endregion

        #region repl
        public void RunInteractive()
        {
            LoadHistory();

            //Python.InitVariables(new List<string>() { "repl", "test", "output", "info", "table" });

            string code = "";
            while (false == code.StartsWith(":q"))
            {
                if (false == string.IsNullOrEmpty(code) && false == code.StartsWith(":"))
                {
                    ScriptSource source = Python.Engine.CreateScriptSourceFromString(code);
                    WriteCode(code);
                    Python.RunCode(source, code);

                    //Python.CheckVariables();
                }

                if (false == string.IsNullOrEmpty(code) && true == code.StartsWith(":"))
                {
                    Command command = _commands.GetCommand(code);
                    command.Execute(this, code);

                }
                code = GetCode();
            }

            SaveHistory();
        }

        private void WriteCode(string code)
        {
            if (default(Action<string>) == ReadLine.Output)
            {
                PythonAst ast = Python.GetAst(code, AstParseMode.Interactive);
                if (default(PythonAst) != ast)
                {
                    List<TextChunk> nodes = ast.Body.HighlightCode().ToList();
                    int lines = nodes.Where(tc => tc.Text == "\n").Count();
                    if (lines > 0)
                    {
                        nodes.Add(new TextChunk("            "));
                    }
                    //if(true == Output.EnablePaging)
                    //{
                        Console.SetCursorPosition(0, Console.CursorTop - 1 - lines);
                    //}
                    foreach (TextChunk chunk in nodes)
                    {
                        ConsoleUtils.Write(chunk.Text, chunk.Color);
                    }
                    ConsoleUtils.Write("\n");
                }
            }
        }

        private string GetCode()
        {
            string result = "";
            bool codeComplete = false;
            int indent = 0;

            while (false == codeComplete)
            {
                System.Console.Write("".PadLeft(indent, '>'));
                if (indent > 0)
                {
                    System.Console.Write(" ");
                }
                string line = GetCodeLine();
                result = $"{result}{"".PadLeft(indent * 2, ' ')}{line}{Environment.NewLine}";
                if (line.EndsWith(":", StringComparison.InvariantCulture) || line.EndsWith("\\", StringComparison.InvariantCulture))
                {
                    indent++;
                }
                else
                {
                    if (true == string.IsNullOrEmpty(line) && indent > 0)
                    {
                        indent--;
                    }
                    codeComplete = 0 == indent;
                }

            }
            return result;
        }
        //private int _cursorPosition = 0;
        private string GetCodeLine()
        {
            return ReadLine.Read("");
        }
        #endregion

        public T New<T>(PythonDictionary dictionary = default(PythonDictionary))
        {
            try
            {
                T result = (T)typeof(T).GetConstructor(new Type[] { }).Invoke(new object[] { });
                if (default(PythonDictionary) != dictionary)
                {
                    Dictionary<string, object> properties = new Dictionary<string, object>();
                    foreach (object key in dictionary.Keys)
                    {
                        properties[key.ToString().ToLower()] = dictionary[key];
                    }

                    PropertyInfo[] propertyInfo = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);

                    foreach (PropertyInfo property in propertyInfo)
                    {
                        string propertyName = property.Name.ToLower();
                        if (true == properties.ContainsKey(propertyName))
                        {
                            object value = properties[propertyName];
                            property.SetValue(result, value);
                        }
                    }
                }

                return result;
            }
            catch
            {
                return default(T);
            }
        }

        #region table
        public void Table(object obj)
        {
            List<List<string>> table = new List<List<string>>();
            Type variableType = (Type)obj.GetType();
            if (typeof(IEnumerable<object>).IsAssignableFrom(variableType))
            {
                foreach (object item in obj as IEnumerable<object>)
                {
                    variableType = (Type)item.GetType();

                    if (variableType.Name == "PythonDictionary")
                    {
                        PythonDictionary dictionary = item as PythonDictionary;
                        table.Add(DictToTableRow(dictionary));
                        continue;
                    }
                    if (typeof(IEnumerable<object>).IsAssignableFrom(variableType))
                    {
                        table.Add(ListToTableRow(item as IEnumerable<object>));
                        continue;
                    }

                    table.Add(ObjectToTableRow(item));
                }
            }

            Output.WriteTable(table);
        }

        private List<string> DictToTableRow(PythonDictionary dictionary)
        {
            List<string> row = new List<string>();
            foreach (object key in dictionary.Keys)
            {
                row.Add($"{dictionary[key]}");
            }

            return row;
        }

        private List<string> ListToTableRow(IEnumerable<object> list)
        {
            List<string> row = new List<string>();
            foreach (object item in list)
            {
                row.Add(item.ToString());
            }

            return row;
        }

        private List<string> ObjectToTableRow(object obj)
        {
            List<string> row = new List<string>();

            Type variableType = (Type)obj.GetType();

            PropertyInfo[] propertyInfo = variableType.GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);

            if (propertyInfo.Any())
            {
                int width = propertyInfo.Max(p => p.Name.Length);
                foreach (PropertyInfo property in propertyInfo)
                {
                    try
                    {
                        object value = property.GetValue(obj);
                        if (null != value && (false == string.IsNullOrWhiteSpace(value.ToString())))
                        {
                            row.Add(value.ToString());
                        }
                    }
                    catch (Exception e)
                    {
                    }
                }
            }
            else
            {
                row.Add(obj.ToString());
            }

            return row;
        }
        #endregion

        public LinqExpression FuncToExpression<T>(string function)
        {
            AstNodeExpressionGenerators.Python = Python;
            return AstNodeExpressionGenerators.CreateExpressionFromFunction<T>(function);
        }

        public UnpackedOption UnpackOption<TType, TError>(Option<TType, TError> option)
        {
            UnpackedOption result = new UnpackedOption();

            option.Match(
                ok => result.Success = ok,
                error => result.Error = error
            );

            return result;
        }

        public void Info(Type type, bool alphabetical = false)
        {
            List<List<string>> output = new List<List<string>>();

            MethodInfo[] methodInfo = type.GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);

            var methods = methodInfo.Where(m => !(m.Name.StartsWith("get_") || m.Name.StartsWith("set_")))
                                    .Select(mi => mi.Name)
                                    .ToList();

            IEnumerable<PropertyInfo> propertyInfo = type.GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly).ToList();

            if (true == alphabetical)
            {
                propertyInfo = propertyInfo.OrderBy(p => p.Name);
            }

            foreach (PropertyInfo property in propertyInfo)
            {
                List<string> pi = new List<string>() {
                    property.Name,
                    property.PropertyType.GetTypeInfo().Name,
                    ColumnInfo(property),
                };
                List<string> extraPropertyInfo = FieldInfo(property);
                if (default(List<string>) != extraPropertyInfo)
                {
                    pi.AddRange(extraPropertyInfo);
                }

                output.Add(pi);
            }

            Output.WriteTable(output);
            var cursor = Console.CursorTop;

            string ColumnInfo(PropertyInfo p)
            {
                string result = "";
                ColumnAttribute column = p.GetCustomAttribute<ColumnAttribute>();

                if (default(ColumnAttribute) != column)
                {
                    result = column.Name;
                }

                return result;
            }
        }

    }
}
