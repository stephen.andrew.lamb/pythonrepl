﻿namespace PythonRepl
{
    public class UnpackedOption
    {
        public object Success { get; set; }
        public object Error { get; set; }
    }
}